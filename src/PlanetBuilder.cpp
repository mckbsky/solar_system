#include <PlanetBuilder.hpp>

PlanetBuilder *PlanetBuilder::builder() {
    return new PlanetBuilder();
}

PlanetBuilder *PlanetBuilder::setType(Planet type) {
    this->type = type;
    return this;
}

PlanetBuilder *PlanetBuilder::setTranslation(Point3D *translation) {
    this->translation = translation;
    return this;
}

PlanetBuilder *PlanetBuilder::setParentTranslation(Point3D *parentTranslation) {
    this->parentTranslation = parentTranslation;
    return this;
}

PlanetBuilder *PlanetBuilder::setRadius(double radius) {
    this->radius = radius;
    return this;
}

PlanetBuilder *PlanetBuilder::setGyrationDelta(GLfloat gyrationDelta) {
    this->gyrationDelta = gyrationDelta;
    return this;
}

PlanetBuilder *PlanetBuilder::setRadiusRatio(GLfloat radiusRatio) {
    this->radiusRatio = radiusRatio;
    return this;
}

PlanetBuilder *PlanetBuilder::setCirculationDelta(GLfloat circulationDelta) {
    this->circulationDelta = circulationDelta;
    return this;
}

PlanetBuilder *PlanetBuilder::setRing(Ring *ring) {
    this->ring = ring;
    return this;
}

Sphere *PlanetBuilder::build() {
    return new Sphere(type, translation, radius, gyrationDelta, circulationDelta, radiusRatio, parentTranslation, ring);
}
