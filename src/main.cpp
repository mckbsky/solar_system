#include <Engine.hpp>

int main(int argc, char **argv) {
    auto engine = Engine::getInstance();
    engine->init(argc, argv);
    engine->mainLoop();

    return 0;
}
