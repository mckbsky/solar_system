#include <Camera.hpp>
#include <glut.h>

Camera::Camera() {
    position = new Point3D(0.0, 0, -450.0);
    center = new Point3D(0, 0, -1.0);
    angleY = -35.0;
    moveSpeed = 5.0;
}

void Camera::setLookAt() {
    gluLookAt(position->getX(), position->getY(), position->getZ(),
              center->getX(), center->getY(), center->getZ(), 0, 1, 0);
}

Point3D *Camera::getPosition() {
    return this->position;
}

Point3D *Camera::getCenter() {
    return this->center;
}

GLdouble Camera::getAngleX() {
    return this->angleX;
}

GLdouble Camera::getAngleY() {
    return this->angleY;
}

void Camera::tiltLeft() {
    angleX -= angleDelta;
}

void Camera::tiltRight() {
    angleX += angleDelta;
}

void Camera::tiltUp() {
    angleY -= angleDelta;
}

void Camera::tiltDown() {
    angleY += angleDelta;
}

void Camera::goLeft() {
    position->shift({moveSpeed, 0, 0});
    center->shift({moveSpeed, 0, 0});
}

void Camera::goRight() {
    position->shift({-moveSpeed, 0, 0});
    center->shift({-moveSpeed, 0, 0});
}

void Camera::goIn() {
    position->shift({0, 0, moveSpeed});
}

void Camera::goOut() {
    position->shift({0, 0, -moveSpeed});
}

