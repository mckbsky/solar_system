#include <Orbit.hpp>
#include <cmath>

Orbit::Orbit(GLdouble majorRadius, GLdouble radiusRatio)
        : majorRadius(majorRadius) {
    this->minorRadius = majorRadius * radiusRatio;
    this->deltaRadius = 1.0 / majorRadius;
    initOrbitPoints();
}


void Orbit::initOrbitPoints() {
    GLdouble angle = 0.0;
    while(angle < 2 * M_PI) {
        orbitPoints.emplace_back(majorRadius * sin(angle), 0, minorRadius * cos(angle));
        angle += deltaRadius;
    }
}

void Orbit::draw() {
    glDisable(GL_TEXTURE_2D);
    glColor3f(0.9, 0.0, 0.1);
    glBegin(GL_POINTS);
        for (auto point : orbitPoints) {
            glVertex3d(point.getX(), 0.0, point.getZ());
        }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
}

GLdouble Orbit::getMajorRadius() const {
    return majorRadius;
}

GLdouble Orbit::getMinorRadius() const {
    return minorRadius;
}

