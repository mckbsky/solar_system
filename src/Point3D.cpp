#include <Point3D.hpp>

Point3D::Point3D(GLdouble x, GLdouble y, GLdouble z)
        : x(x), y(y), z(z) {}

GLdouble Point3D::getX() {
    return this->x;
}

GLdouble Point3D::getY() {
    return this->y;
}

GLdouble Point3D::getZ() {
    return this->z;
}

void Point3D::setX(GLdouble x) {
    this->x = x;
}

void Point3D::setY(GLdouble y) {
    this->y = y;
}

void Point3D::setZ(GLdouble z) {
    this->z = z;
}

Point3D* Point3D::shift(Point3D point3D) {
    x = x + point3D.getX();
    y = y + point3D.getY();
    z = z + point3D.getZ();
    return this;
}

