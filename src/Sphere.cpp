#include <Sphere.hpp>

Sphere::Sphere(Planet type, Point3D *translation, GLdouble radius, GLfloat gyrationDelta, GLfloat circulationDelta,
               GLfloat radiusRatio, Point3D *parentTranslation, Ring * ring)
        : type(type), translation(translation), radius(radius), parentTranslation(parentTranslation), ring(ring) {
    orbit = type == Planet::SUN ? nullptr : new Orbit(translation->getX(), radiusRatio);
    rotation = new Rotation(gyrationDelta, circulationDelta, translation, orbit, parentTranslation);
    bindTexture();
}

void Sphere::bindTexture() {
    gluQuadric = gluNewQuadric();
    texture = new Texture();
    glGenTextures(1, texture->getId());
    glBindTexture(GL_TEXTURE_2D, *texture->getId());
    texture->loadTexture(const_cast<char *>(Texture::getTextureName(type).c_str()));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void Sphere::draw() {
    glPushMatrix();
    glTranslated(translation->getX(), translation->getY(), translation->getZ());
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, *texture->getId());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glRotatef(90, 1.0f, 0.0f, 0.0f);
    glRotated(rotation ? rotation->getGyration() : 0.0, 0.0f, 0.0f, 1.0f);
    gluQuadricTexture(gluQuadric, 1);
    gluSphere(gluQuadric, radius, 30, 20);
    glPopMatrix();
}

void Sphere::rotate() {
    this->rotation->rotate();
    if(!moons.empty()) {
        for(auto moon : moons) {
            moon->rotation->rotate();
        }
    }
}

void Sphere::drawOrbit() {
    if(orbit != nullptr) {
        this->orbit->draw();
    }
}

void Sphere::drawMoons() {
    if(!moons.empty()) {
        for(auto moon : moons) {
            moon->draw();
        }
    }
}

void Sphere::drawRing() {
    if(ring != nullptr) {
        this->ring->draw();
    }
}

Point3D *Sphere::getTranslation() {
    return this->translation;
}

void Sphere::addMoon(Sphere *moon) {
    moons.emplace_back(moon);
}

GLdouble Sphere::getRadius() {
    return this->radius;
}
