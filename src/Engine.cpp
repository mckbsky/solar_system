
#include <Engine.hpp>
#include <GL/glut.h>

Engine *Engine::instance = nullptr;

Engine::Engine() {
    this->keyboard = new Keyboard();
    this->camera = new Camera();
    this->timer = new Timer();
}

Engine *Engine::getInstance() {
    if (instance == nullptr) {
        instance = new Engine();
    }
    return instance;
}

void Engine::init(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow(argv[0]);
    glutDisplayFunc(displayWrapper);
    glutIdleFunc(displayWrapper);
    glutReshapeFunc(reshapeWrapper);
    glutKeyboardFunc(keyboardWrapper);
    glutSpecialFunc(keyboardSpecialWrapper);
    initRendering();
    glutTimerFunc(25, updateWrapper, 0);
    this->scene = new Scene();
}

void Engine::initRendering() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Engine::mainLoop() {
    glutMainLoop();
}

void Engine::reshape(int w, int h) {
    h = h == 0 ? 1 : h;
    auto ratio = static_cast<float>(w * 1.0 / h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluPerspective(45.0f, ratio, 1.0f, 3000.0f);
    glMatrixMode(GL_MODELVIEW);
}

void Engine::displayWrapper() {
    Engine::getInstance()->getScene()->draw();
}

void Engine::reshapeWrapper(int width, int height) {
    Engine::getInstance()->reshape(width, height);
}

void Engine::updateWrapper(int value) {
    Timer::update(value);
}

void Engine::keyboardWrapper(unsigned char key, int x, int y) {
    Engine::getInstance()->keyboard->handle(key, x, y);
}

void Engine::keyboardSpecialWrapper(int key, int x, int y) {
    Engine::getInstance()->keyboard->handleSpecialKeys(key, x, y);
}

Scene *Engine::getScene() {
    return this->scene;
}

Camera *Engine::getCamera() {
    return this->camera;
}

Engine::~Engine() {
    delete keyboard;
}

Timer *Engine::getTimer() {
    return this->timer;
}
