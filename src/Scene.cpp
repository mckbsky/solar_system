#include <gl.h>
#include <glut.h>
#include <Scene.hpp>
#include <Engine.hpp>
#include <PlanetFactory.hpp>


void Scene::draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    Engine::getInstance()->getCamera()->setLookAt();
    glRotated(Engine::getInstance()->getCamera()->getAngleX(), 0, 1.0f, 0);
    glRotated(Engine::getInstance()->getCamera()->getAngleY(), 1.0f, 0, 0);

    drawStars();
    drawPlanets();

    glFlush();
    glutSwapBuffers();
}

Scene::Scene() {
    initPlanets();
    initStars(1000, 300);
}

void Scene::initPlanets() {
    planets.insert(std::pair<Planet, Sphere *>(Planet::SUN, PlanetFactory::getPlanet(Planet::SUN)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::MERCURY, PlanetFactory::getPlanet(Planet::MERCURY)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::VENUS, PlanetFactory::getPlanet(Planet::VENUS)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::EARTH, PlanetFactory::getPlanet(Planet::EARTH)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::MARS, PlanetFactory::getPlanet(Planet::MARS)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::JUPITER, PlanetFactory::getPlanet(Planet::JUPITER)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::SATURN, PlanetFactory::getPlanet(Planet::SATURN)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::URANUS, PlanetFactory::getPlanet(Planet::URANUS)));
    planets.insert(std::pair<Planet, Sphere *>(Planet::NEPTUNE, PlanetFactory::getPlanet(Planet::NEPTUNE)));
}

const std::map<Planet, Sphere *> &Scene::getPlanets() const {
    return planets;
}

void Scene::initStars(int numberOfStars, GLdouble minPoint) {
    auto p = [](GLdouble minPoint) -> double {return (rand() % 700 + minPoint) * (-1 + 2 * (float) rand() / RAND_MAX);};

    for (int i = 0; i < numberOfStars; i++)
        stars.emplace_back(p(minPoint), p(minPoint), p(minPoint));
}

void Scene::drawPlanets() const {
    for (auto &planet: planets) {
        planet.second->draw();
        planet.second->drawOrbit();
        planet.second->drawMoons();
        planet.second->drawRing();
    }
}


void Scene::drawStars() {
    glPointSize(1.0);
    glBegin(GL_POINTS);
        for (auto star : stars) {
            glVertex3d(star.getX(), star.getY(), star.getZ());
        }
    glEnd();
}

