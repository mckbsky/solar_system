#include "Ring.hpp"

Ring::Ring(GLdouble inner, GLdouble outer, GLint slices, GLint loops, Point3D *parentTranslation)
        : inner(inner), outer(outer), slices(slices), loops(loops), translation(parentTranslation) {
    gluQuadric = gluNewQuadric();
    bindTexture();
}

void Ring::draw() {
    glPushMatrix();
    glTranslated(translation->getX(), translation->getY(), translation->getZ());
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, *texture->getId());
    gluQuadricTexture(gluQuadric, 1);
    glRotated(90, 1, 0, 0);
    gluDisk(gluQuadric, inner, outer, slices, loops);
    glPopMatrix();
}

void Ring::bindTexture() {
    texture = new Texture();
    glGenTextures(1, texture->getId());
    glBindTexture(GL_TEXTURE_2D, *texture->getId());
    texture->loadTexture(const_cast<char *>(Texture::getTextureName(Planet::SATURN_RING).c_str()));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

