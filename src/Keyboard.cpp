#include <Keyboard.hpp>
#include <cstdlib>
#include <Engine.hpp>
#include <glut.h>

void Keyboard::handle(unsigned char key, int x, int y) {

    auto camera = Engine::getInstance()->getCamera();
    auto timer = Engine::getInstance()->getTimer();

    switch (key) {
        case 'w':
            camera->goIn();
            break;
        case 's':
            camera->goOut();
            break;
        case 'd':
            camera->goRight();
            break;
        case 'a':
            camera->goLeft();
            break;
        case '-':
            timer->decreaseTimeScale();
            break;
        case '=':
            timer->increaseTimeScale();
            break;
        case 27 :
        case 'q':
            exit(0);
        default:
            break;
    }
}

void Keyboard::handleSpecialKeys(int key, int xx, int yy) {

    auto camera = Engine::getInstance()->getCamera();

    switch (key) {
        case GLUT_KEY_UP :
            camera->tiltUp();
            break;
        case GLUT_KEY_DOWN :
            camera->tiltDown();
            break;
        case GLUT_KEY_LEFT :
            camera->tiltRight();
            break;
        case GLUT_KEY_RIGHT:
            camera->tiltLeft();
            break;
        default:
            break;
    }
}
