#include <Texture.hpp>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>

std::map<Planet, std::string> Texture::textures = {
        {Planet::SUN,         "sun.bmp"},
        {Planet::MERCURY,     "mercury.bmp"},
        {Planet::VENUS,       "venus.bmp"},
        {Planet::EARTH,       "earth.bmp"},
        {Planet::MOON,        "moon.bmp"},
        {Planet::MARS,        "mars.bmp"},
        {Planet::JUPITER,     "jupiter.bmp"},
        {Planet::SATURN,      "saturn.bmp"},
        {Planet::SATURN_RING, "saturn-ring.bmp"},
        {Planet::URANUS,      "uranus.bmp"},
        {Planet::NEPTUNE,     "neptune.bmp"}
};

unsigned char *Texture::loadImage(char *filename, int *width, int *height) {
    auto image = cvLoadImage(filename);
    if (nullptr == image) {
        std::cerr << "Error loading texture: " << filename << std::endl;
        return nullptr;
    }
    uchar *ptr;
    auto *data = new GLubyte[image->height * image->width * 3];

    int i = 0;
    *width = image->width;
    *height = image->height;
    for (int y = 0; y < image->height; y++) {
        for (int x = 0; x < image->width; x++) {
            ptr = cvPtr2D(image, y, x, nullptr);
            data[i++] = ptr[2];
            data[i++] = ptr[1];
            data[i++] = ptr[0];
        }
    }
    cvReleaseImage(&image);
    return data;
}

bool Texture::loadTexture(char *filename) {
    int w = 0, h = 0;
    auto pixels = loadImage(filename, &w, &h);
    if (pixels == nullptr)
        return false;
    glTexImage2D(GL_TEXTURE_2D, 0, 3, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
    delete pixels;
    return true;
}

Texture::Texture() {
    this->id = new GLuint;
}

std::string Texture::getTextureName(Planet planet) {
    return textures.at(planet);
}

GLuint *Texture::getId() const {
    return id;
}

