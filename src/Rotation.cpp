#include <gl.h>
#include <Rotation.hpp>
#include <cmath>

Rotation::Rotation(GLfloat gyrationDelta, GLfloat circulationDelta, Point3D *translation, Orbit *orbit, Point3D* parentTranslation)
        : circulation(0.0), gyration(0.0), translation(translation), orbit(orbit), parentTranslation(parentTranslation){
    this->gyrationDelta = gyrationDelta;
    this->circulationDelta = circulationDelta;
}

void Rotation::rotate() {
    gyration += gyrationDelta;
    circulation += circulationDelta;

    if (orbit != nullptr) {
        if(parentTranslation != nullptr) {
            translation->setX(parentTranslation->getX() + orbit->getMajorRadius() * cos(circulation));
            translation->setZ(parentTranslation->getZ() + orbit->getMinorRadius() * sin(circulation));
        }
        else {
            translation->setX(orbit->getMajorRadius() * cos(circulation));
            translation->setZ(orbit->getMinorRadius() * sin(circulation));
        }
    }

    if (gyration > 360.f) {
        gyration -= 360;
    }
    if (circulation > 360.f) {
        circulation -= 360;
    }
}

GLfloat Rotation::getGyration() const {
    return gyration;
}


