#include <PlanetBuilder.hpp>
#include <PlanetFactory.hpp>

Sphere *PlanetFactory::getPlanet(Planet planet) {
    switch (planet) {
        case SUN : {
            return PlanetBuilder::builder()
                    ->setType(Planet::SUN)
                    ->setTranslation(new Point3D(0.0, 0.0, -5.0))
                    ->setRadius(45)
                    ->setGyrationDelta(0.05)
                    ->setCirculationDelta(0.0)
                    ->setRadiusRatio(0.66)
                    ->build();
        }
        case MERCURY: {
            return PlanetBuilder::builder()
                    ->setType(Planet::MERCURY)
                    ->setTranslation(new Point3D(90.0, 0.0, -5.0))
                    ->setRadius(8)
                    ->setGyrationDelta(0.5)
                    ->setCirculationDelta(0.03)
                    ->setRadiusRatio(0.66)
                    ->build();
        }
        case VENUS: {
            return PlanetBuilder::builder()
                    ->setType(Planet::VENUS)
                    ->setTranslation(new Point3D(140.0, 0.0, -5.0))
                    ->setRadius(14)
                    ->setGyrationDelta(0.2)
                    ->setCirculationDelta(0.028)
                    ->setRadiusRatio(0.66)
                    ->build();
        }
        case EARTH: {
            auto earth = PlanetBuilder::builder()
                    ->setType(Planet::EARTH)
                    ->setTranslation(new Point3D(190.0, 0.0, -5.0))
                    ->setRadius(18)
                    ->setGyrationDelta(3.0)
                    ->setCirculationDelta(0.023)
                    ->setRadiusRatio(0.66)
                    ->build();

            earth->addMoon(PlanetBuilder::builder()
                    ->setType(Planet::MOON)
                    ->setTranslation(new Point3D(earth->getRadius() + 5, 5.0, 0.0))
                    ->setParentTranslation(earth->getTranslation())
                    ->setRadius(2.5)
                    ->setGyrationDelta(3.0)
                    ->setCirculationDelta(0.1)
                    ->setRadiusRatio(0.66)
                    ->build());

            return earth;
        }
        case MARS: {
            auto mars = PlanetBuilder::builder()
                    ->setType(Planet::MARS)
                    ->setTranslation(new Point3D(240.0, 0.0, -5.0))
                    ->setRadius(12)
                    ->setGyrationDelta(2.8)
                    ->setCirculationDelta(0.02)
                    ->setRadiusRatio(0.66)
                    ->build();

            mars->addMoon(PlanetBuilder::builder()
                    ->setType(Planet::MOON)
                    ->setTranslation(new Point3D(mars->getRadius() + 7, 7.0, 0.0))
                    ->setParentTranslation(mars->getTranslation())
                    ->setRadius(2.5)
                    ->setGyrationDelta(3.0)
                    ->setCirculationDelta(0.05)
                    ->setRadiusRatio(0.66)
                    ->build());

            mars->addMoon(PlanetBuilder::builder()
                    ->setType(Planet::MOON)
                    ->setTranslation(new Point3D(mars->getRadius() + 5, 4.0, 15.0))
                    ->setParentTranslation(mars->getTranslation())
                    ->setRadius(2.5)
                    ->setGyrationDelta(3.0)
                    ->setCirculationDelta(0.07)
                    ->setRadiusRatio(0.66)
                    ->build());

            return mars;
        }
        case JUPITER: {
            return PlanetBuilder::builder()
                    ->setType(Planet::JUPITER)
                    ->setTranslation(new Point3D(360.0, 0.0, -5.0))
                    ->setRadius(23)
                    ->setGyrationDelta(3.5)
                    ->setCirculationDelta(0.017)
                    ->setRadiusRatio(0.66)
                    ->build();
        }
        case SATURN: {
            auto *translation = new Point3D(450.0, 0.0, -5.0);

            return PlanetBuilder::builder()
                    ->setType(Planet::SATURN)
                    ->setTranslation(translation)
                    ->setRadius(16)
                    ->setGyrationDelta(3.4)
                    ->setCirculationDelta(0.013)
                    ->setRadiusRatio(0.66)
                    ->setRing(new Ring(30.0, 40.0, 40, 40, translation))
                    ->build();
        }
        case URANUS: {
            return PlanetBuilder::builder()
                    ->setType(Planet::URANUS)
                    ->setTranslation(new Point3D(700.0, 0.0, -5.0))
                    ->setRadius(16)
                    ->setGyrationDelta(2.4)
                    ->setCirculationDelta(0.01)
                    ->setRadiusRatio(0.66)
                    ->build();
        }
        case NEPTUNE: {
            return PlanetBuilder::builder()
                    ->setType(Planet::NEPTUNE)
                    ->setTranslation(new Point3D(800.0, 0.0, -5.0))
                    ->setRadius(10)
                    ->setGyrationDelta(2.5)
                    ->setCirculationDelta(0.007)
                    ->setRadiusRatio(0.66)
                    ->build();
        }
    }
}
