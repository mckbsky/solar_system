#include <glut.h>
#include <Engine.hpp>

unsigned int Timer::timeScale = 25;
unsigned int Timer::timeDelta = 5;

void Timer::update(__attribute__((unused)) int value) {
    for (auto planet : Engine::getInstance()->getScene()->getPlanets()) {
        planet.second->rotate();
    }

    glutPostRedisplay();
    glutTimerFunc(timeScale, update, 0);
}

void Timer::increaseTimeScale() {
    if(timeScale - timeDelta >= timeDelta) {
        timeScale -= timeDelta;
    }
}

void Timer::decreaseTimeScale() {
    if(timeScale + timeDelta <= 100) {
        timeScale += timeDelta;
    }
}
