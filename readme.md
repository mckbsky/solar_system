<center>Grafika komputerowa - projekt</center>

---

<center>Politechika Świętokrzyska</center>
<center>Wydział Elektrotechniki, Automatyki i Informatyki</center>
<center>Informatyka rok 3, semestr 5</center>

<center>Autorzy: Maciej Brzęczkowski, Adam Bębenek</center>

<center>Układ słoneczny - symulacja</center>

---

Tematem projektu było wykonanie symulacji układu słonecznego z wykorzystaniem api OpenGl oraz biblioteki glut i języka C++. W skład symulacji wchodzi 8 niezależnych planet + słońce, orbity, gwiazdy, pierścienie oraz księżyce. Program zawiera obsługę kamery (przesunięcie, obrót) przy pomocy klawiatury. Planety obracają się ruchem obiegowym i obrotowym z narzuconą prędkością.

<center>Instrukcja kompilacji/uruchomienia, opis działania i instrukcja obsługi</center>

---------------------

Projekt zawiera plik CMakeLists.txt, któru umożliwia wygenerowanie plików projektowych. Sterowanie kamerą możliwe jest przy pomocy klawiszy 'wasd', obrót kamery realizowany jest przy pomocy strzałek.

<center>Zrzuty ekranu z przykładowym zadziałaniem</center>

---------------------

<img src="gfx/ss1.png" alt="ss1" align="center"/>  
<img src="gfx/ss2.png" alt="ss1" align="center"/>

<center>Opis użytych algorytmów i najważniejszych fragmentów implementacji</center>

---------------------

##### Engine
Główną klasą w programie jest klasa Engine, która inicjalizuje glut'a oraz tworzy obiekt Scene, w którym odbywa się rysowanie sceny
```c++
void Engine::init(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow(argv[0]);
    glutDisplayFunc(displayWrapper);
    glutIdleFunc(displayWrapper);
    glutReshapeFunc(reshapeWrapper);
    glutKeyboardFunc(keyboardWrapper);
    glutSpecialFunc(keyboardSpecialWrapper);
    initRendering();
    glutTimerFunc(25, updateWrapper, 0);
    this->scene = new Scene();
}

void Engine::initRendering() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
```
##### Keyboard
W klasie keyboard odbywa się bindowanie akcji kamery od przycisków na klawiaturze

```c++
void Keyboard::handleSpecialKeys(int key, int xx, int yy) {
    Camera *camera = Engine::getInstance()->getCamera();

    switch (key) {
        case GLUT_KEY_UP :
            camera->tiltUp();
            break;
        case GLUT_KEY_DOWN :
            camera->tiltDown();
            break;
        case GLUT_KEY_LEFT :
            camera->tiltRight();
            break;
        case GLUT_KEY_RIGHT:
            camera->tiltLeft();
            break;
        default:
            break;
    }
}
```

##### Camera
W klasie Camera ustawiany jest gluLookAt odpowiedzialny za pozycję kamery

```c++
void Camera::setLookAt() {
    gluLookAt(position->getX(), position->getY(), position->getZ(),
              center->getX(), center->getY(), center->getZ(), 0, 1, 0);
}
```

##### Scene
Klasa Scene zawiera główną metodę rysującą

```c++
void Scene::draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    Engine::getInstance()->getCamera()->setLookAt();
    glRotated(Engine::getInstance()->getCamera()->getAngleX(), 0, 1.0f, 0);
    glRotated(Engine::getInstance()->getCamera()->getAngleY(), 1.0f, 0, 0);

    drawStars();
    drawPlanets();

    glFlush();
    glutSwapBuffers();
}
```

##### PlanetFactory
Planety dodawane są do sceny przy pomocy klasy PlanetFactory

```c++
switch(planet) {
...
    case VENUS: {
            return PlanetBuilder::builder()
                  ->setType(Planet::VENUS)
                  ->setTranslation(new Point3D(140.0, 0.0, -5.0))
                  ->setRadius(14)
                  ->setGyrationDelta(0.2)
                  ->setCirculationDelta(0.028)
                  ->setRadiusRatio(0.66)
                  ->build();
        }
        case EARTH: {
            Sphere* earth = PlanetBuilder::builder()
                  ->setType(Planet::EARTH)
                  ->setTranslation(new Point3D(190.0, 0.0, -5.0))
                  ->setRadius(18)
                  ->setGyrationDelta(3.0)
                  ->setCirculationDelta(0.023)
                  ->setRadiusRatio(0.66)
                  ->build();

            earth->addMoon(PlanetBuilder::builder()
                  ->setType(Planet::MOON)
                  ->setTranslation(new Point3D(earth->getRadius() + 5, 5, 0))
                  ->setParentTranslation(earth->getTranslation())
                  ->setRadius(2.5)
                  ->setGyrationDelta(3.0)
                  ->setCirculationDelta(0.1)
                  ->setRadiusRatio(0.66)
                  ->build());

            return earth;
        }
...
}
```
##### Sphere
Rysowanie sfery przy wykorzystaniu gluSphere()

```c++
void Sphere::draw() {
    glPushMatrix();
    glTranslated(translation->getX(), translation->getY(), translation->getZ());
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, *texture->getId());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glRotatef(90, 1.0f, 0.0f, 0.0f);
    glRotated(rotation ? rotation->getGyration() : 0.0, 0.0f, 0.0f, 1.0f);
    gluQuadricTexture(gluQuadric, 1);

    gluSphere(gluQuadric, radius, 30, 20);
    glPopMatrix();
}
```

##### Rotation
Ruch obrotowy i obiegowy (ze wzoru elipsy)

```c++
void Rotation::rotate() {
    gyration += gyrationDelta;
    circulation += circulationDelta;

    if (orbit != nullptr) {
        if(parentTranslation != nullptr) {
            translation->setX(parentTranslation->getX() +
            orbit->getMajorRadius() * cos(circulation));
            translation->setZ(parentTranslation->getZ() +
            orbit->getMinorRadius() * sin(circulation));
        }
        else {
            translation->setX(orbit->getMajorRadius() * cos(circulation));
            translation->setZ(orbit->getMinorRadius() * sin(circulation));
        }
    }

    if (gyration > 360.f) {
        gyration -= 360;
    }
    if (circulation > 360.f) {
        circulation -= 360;
    }
}
```

##### Orbit
Orbita rysowana jest po punktach, a ich pozycje wyliczane są ze wzoru na elipsę

```c++
void Orbit::initOrbitPoints() {
    GLdouble angle = 0.0;
    while(angle < 2 * M_PI) {
        orbitPoints.emplace_back(majorRadius * sin(angle), 0,
        minorRadius * cos(angle));
        angle += deltaRadius;
    }
}

void Orbit::draw() {
    glDisable(GL_TEXTURE_2D);
    glColor3f(0.9, 0.0, 0.1);
    glBegin(GL_POINTS);
        for (auto point : orbitPoints) {
            glVertex3d(point.getX(), 0.0, point.getZ());
        }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
}
```

##### Ring
Pierścień Saturna rysowany jest przy wykorzystaniu gluDisk()

```c++
void Ring::draw() {
    glPushMatrix();
    glTranslated(translation->getX(), translation->getY(), translation->getZ());
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, *texture->getId());
    gluQuadricTexture(gluQuadric, 1);

    glRotated(90, 1, 0, 0);
    gluDisk(gluQuadric, inner, outer, slices, loops);

    glPopMatrix();
}
```


<center>Podsumowanie</center>

---------------------

Dzięki projektowi poznaliśmy api OpenGl oraz bibliotekę glut. Z założeń projektu udało się zaimplementować wszystkie funkcjonalności z wyjątkiem oświetlenia. Kolejnymi krokami rozwoju projektu mogłaby być zmiana skali na bardziej realistyczną, dodanie brakujących księżyców oraz implementacja oświetlenia.
