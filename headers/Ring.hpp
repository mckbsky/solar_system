#ifndef RING_HPP_INCLUDED
#define RING_HPP_INCLUDED

#include <gl.h>
#include <glu.h>
#include "Point3D.hpp"
#include "Texture.hpp"

class Ring {
private:
    GLdouble inner;
    GLdouble outer;
    GLint slices;
    GLint loops;
    GLUquadric *gluQuadric;
    Point3D *translation;
    Texture *texture;

public:
    Ring(GLdouble, GLdouble, GLint, GLint, Point3D*);

    void draw();

    void bindTexture();

};

#endif
