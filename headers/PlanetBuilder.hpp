#ifndef SOLAR_SYSTEM_PLANERBUILDER_HPP
#define SOLAR_SYSTEM_PLANERBUILDER_HPP

#include "Planet.hpp"
#include "Point3D.hpp"
#include "Sphere.hpp"

class PlanetBuilder {
private:
    Planet type;
    Point3D *translation;
    Point3D *parentTranslation;
    GLdouble radius;
    GLfloat radiusRatio;
    GLfloat gyrationDelta;
    GLfloat circulationDelta;
    Ring *ring;

public:
    static PlanetBuilder *builder();

    PlanetBuilder *setType(Planet);

    PlanetBuilder *setTranslation(Point3D *);

    PlanetBuilder *setParentTranslation(Point3D *);

    PlanetBuilder *setRadius(GLdouble);

    PlanetBuilder *setGyrationDelta(GLfloat);

    PlanetBuilder *setCirculationDelta(GLfloat);

    PlanetBuilder *setRadiusRatio(GLfloat);

    PlanetBuilder *setRing(Ring *);

    Sphere *build();

};

#endif
