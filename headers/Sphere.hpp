#ifndef SOLAR_SYSTEM_SPHERE_HPP
#define SOLAR_SYSTEM_SPHERE_HPP

#include <GL/glu.h>
#include "Planet.hpp"
#include "Texture.hpp"
#include "Rotation.hpp"
#include "Point3D.hpp"
#include "Orbit.hpp"
#include "Ring.hpp"

class Sphere {
private:
    Planet type;
    Texture *texture;
    GLUquadric *gluQuadric;
    Point3D *translation;
    Point3D *parentTranslation;
    Rotation *rotation;
    Orbit *orbit;
    const GLdouble radius;
    std::vector<Sphere *> moons;
    Ring *ring;

public:
    Sphere(Planet, Point3D *, GLdouble, GLfloat, GLfloat, GLfloat, Point3D *, Ring *);

    void draw();

    void drawOrbit();

    void drawMoons();

    void drawRing();

    void addMoon(Sphere *);

    void bindTexture();

    void rotate();

    Point3D *getTranslation();

    GLdouble getRadius();
};

#endif
