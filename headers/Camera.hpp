#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include <Point3D.hpp>

class Camera {
private:
    Point3D *position;
    Point3D *center;
    GLdouble angleX;
    GLdouble angleY;
    const GLdouble angleDelta = 1.0;
    GLdouble moveSpeed;

public:

    Camera();

    Point3D *getPosition();

    Point3D *getCenter();

    GLdouble getAngleX();

    GLdouble getAngleY();

    void tiltLeft();

    void tiltRight();

    void tiltUp();

    void tiltDown();

    void setLookAt();

    void goLeft();

    void goRight();

    void goIn();

    void goOut();

};

#endif
