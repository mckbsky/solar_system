#ifndef SOLAR_SYSTEM_SCENE_HPP
#define SOLAR_SYSTEM_SCENE_HPP

#include <vector>
#include <map>
#include "Sphere.hpp"
#include "Planet.hpp"
#include "Texture.hpp"

class Scene {
private:
    std::map<Planet, Sphere *> planets;
    std::vector<Point3D> stars;
    void initPlanets();

public:
    Scene();

    void draw();

    const std::map<Planet, Sphere *> &getPlanets() const;

    void initStars(int, GLdouble);

    void drawStars();

    void drawPlanets() const;
};

#endif
