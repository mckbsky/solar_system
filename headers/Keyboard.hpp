#ifndef SOLAR_SYSTEM_KEYBOARD_HPP
#define SOLAR_SYSTEM_KEYBOARD_HPP

class Keyboard {
public:
    void handle(unsigned char, int, int);

    void handleSpecialKeys(int, int, int);
};

#endif
