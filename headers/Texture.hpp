#ifndef SOLAR_SYSTEM_TEXTURE_HPP
#define SOLAR_SYSTEM_TEXTURE_HPP

#include <gl.h>
#include <map>
#include "Planet.hpp"

class Texture {
private:
    GLuint *id;
    static std::map<Planet, std::string> textures;
    GLubyte *loadImage(char *filename, int *width, int *height);

public:

    Texture();

    bool loadTexture(char *filename);

    static std::string getTextureName(Planet);

    GLuint *getId() const;
};

#endif
