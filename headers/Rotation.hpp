#ifndef SOLAR_SYSTEM_ROTATION_HPP
#define SOLAR_SYSTEM_ROTATION_HPP


#include <gl.h>
#include <Point3D.hpp>
#include "Orbit.hpp"

class Rotation {
private:
    GLfloat circulation;
    GLfloat circulationDelta;
    GLfloat gyration;
    GLfloat gyrationDelta;
    Point3D *translation;
    Point3D *parentTranslation;
    Orbit *orbit;

public:
    Rotation(GLfloat, GLfloat, Point3D *, Orbit *, Point3D *);

    void rotate();

    GLfloat getGyration() const;

};


#endif
