#ifndef POINT3D_HPP_INCLUDED
#define POINT3D_HPP_INCLUDED

#include <gl.h>

class Point3D {
private:
    GLdouble x;
    GLdouble y;
    GLdouble z;

public:
    Point3D(GLdouble, GLdouble, GLdouble);

    GLdouble getX();

    GLdouble getY();

    GLdouble getZ();

    Point3D* shift(Point3D);

    void setX(GLdouble);

    void setY(GLdouble);

    void setZ(GLdouble);

};

#endif
