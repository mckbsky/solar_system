#ifndef SOLAR_SYSTEM_PLANETFACTORY_HPP
#define SOLAR_SYSTEM_PLANETFACTORY_HPP


#include "Planet.hpp"
#include "Sphere.hpp"

class PlanetFactory {
public:
    static Sphere *getPlanet(Planet);

};


#endif
