#ifndef SOLAR_SYSTEM_ENGINE_HPP
#define SOLAR_SYSTEM_ENGINE_HPP

#include <gl.h>
#include "Keyboard.hpp"
#include "Camera.hpp"
#include "Scene.hpp"
#include "Timer.hpp"

class Engine {
private:
    Keyboard *keyboard;
    Camera *camera;
    Scene *scene;
    Timer *timer;
    static Engine *instance;

    static void displayWrapper();

    static void updateWrapper(int);

    static void reshapeWrapper(int, int);

    static void keyboardWrapper(unsigned char, int, int);

    static void keyboardSpecialWrapper(int, int, int);

    void reshape(int, int);

    void initRendering();

    Engine();

public:
    void init(int, char **);

    void mainLoop();

    static Engine *getInstance();

    ~Engine();

    Camera *getCamera();

    Scene *getScene();

    Timer *getTimer();
};

#endif
