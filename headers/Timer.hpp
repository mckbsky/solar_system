#ifndef SOLAR_SYSTEM_TIMER_HPP
#define SOLAR_SYSTEM_TIMER_HPP


class Timer {
private:

    static unsigned int timeScale;
    static unsigned int timeDelta;

public:

    void static update(int);

    void increaseTimeScale();

    void decreaseTimeScale();

};


#endif
