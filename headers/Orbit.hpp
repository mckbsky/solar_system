#ifndef SOLAR_SYSTEM_ORBIT_HPP
#define SOLAR_SYSTEM_ORBIT_HPP

#include <gl.h>
#include <vector>
#include "Point3D.hpp"

class Orbit {
private:
    GLdouble majorRadius;
    GLdouble minorRadius;
    GLdouble deltaRadius;
    std::vector<Point3D> orbitPoints;

public:

    Orbit(GLdouble, GLdouble);

    void initOrbitPoints();

    void draw();

    GLdouble getMajorRadius() const;

    GLdouble getMinorRadius() const;

};

#endif
